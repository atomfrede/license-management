FROM licensefinder/license_finder:5.5.2
MAINTAINER GitLab

RUN npm install npm-install-peers cheerio

RUN apt-get update -q
RUN apt-get install -y software-properties-common 
RUN add-apt-repository ppa:openjdk-r/ppa 
RUN apt-get update -q 

# Don't let Rubygem fail with the numerous projects using PG or MySQL, install realpath
RUN apt-get update && apt-get install -y openjdk-11-jdk libpq-dev libmysqlclient-dev realpath && rm -rf /var/lib/apt/lists/*

COPY run.sh html2json.js /
COPY test /test

# Don't load RVM automatically, it doesn't work with GitLab-CI
RUN mv /etc/profile.d/rvm.sh /rvm.sh

ENTRYPOINT ["/run.sh"]
