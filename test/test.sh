#!/bin/bash

set -e

for project in ruby-bundler java-maven; do
  # Clone the test repository.
  echo "Cloning the test repository https://gitlab.com/gitlab-org/security-products/tests/$project.git"
  git clone "https://gitlab.com/gitlab-org/security-products/tests/$project.git" "/code/$project"

  # Run license management on it.
  echo "Running license management on the $project"
  cd  "/code/$project"
  /run.sh analyze .

  # Compare results with expected results.
  set +e
  diff "/code/$project/gl-license-management-report.json" "/test/results/$project.json" > /diff.txt
  set -e

  if [ -s /diff.txt ] ; then
    echo "Unexpected result. Here is the diff between actual results and those expected :"
    cat /diff.txt
    exit 1
  fi
done

echo "All tests are OK."
